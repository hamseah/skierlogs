<?php


/* The model class */

class DBModel{

    public $dom;
    protected $db;


    public function __construct(){
        try{
            $this->dom = new DOMDocument();
            $this->dom->load('DBModel/SkierLogs.xml');
            try {
                $this->db = new PDO('mysql:host=localhost; dbname=skierlogs; charset=utf8','root', '');
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $e) {
                echo "Error accured creating new PDO..";
                echo $e->getMessage();
            }
        }catch(Exception $e){
            echo "Error loading file..".'<br>';
            echo $e->getMessage();
        }
    }

    /*  */
    public function main(){

        $this->restoreSeason();
        $this->restoreSkiers();
        $this->restoreClubs();
        $this->restoreTotalDist();

    }

    public function restoreSkiers(){
        $userNames = array();
        $birthyears = array();
        $fNames = array();
        $lNames = array();

        $skiers = $this->dom->getElementsByTagName("Skier");
        $firstname = $this->dom->getElementsByTagName("FirstName");
        $lastname = $this->dom->getElementsByTagName("LastName");
        $years = $this->dom->getElementsByTagName("YearOfBirth");

        foreach($skiers as $skier){
            array_push($userNames, $skier->getAttribute("userName"));
        }
        foreach($firstname as $n){
            array_push($fNames, $n->nodeValue);
        }

        foreach($lastname as $b){
            array_push($lNames, $b->nodeValue);
        }

        foreach($years as $el){
            array_push($birthyears, $el->nodeValue);
        }

        for($i =0; $i <sizeof($birthyears); $i++){

            try {
                $input = $this->db->prepare("INSERT INTO skier(userName, yearOfBirth, firstName, lastName) VALUES (:id, :year, :fname, :lname)");
                $input->bindValue(':id', $userNames[$i], PDO::PARAM_STR);
                $input->bindValue(':year', $birthyears[$i], PDO::PARAM_STR);
                $input->bindValue(':fname', $fNames[$i], PDO::PARAM_STR);
                $input->bindValue(':lname', $lNames[$i], PDO::PARAM_STR);
                $input->execute();
                echo "Success!".'<br>';
            } catch (Exception $e) {
                echo "Failed to update skier data in db!".'<br>';
                echo $e->getMessage();
            }
        }
    }

    public function restoreClubs(){
        $id = array();
        $cnames = array();
        $cities = array();
        $counties = array();

        $clubs = $this->dom->getElementsByTagName("Club");
        $name = $this->dom->getElementsByTagName("Name");
        $city = $this->dom->getElementsByTagName("City");
        $county = $this->dom->getElementsByTagName("County");
        foreach($clubs as $club){
            array_push($id, $club->getAttribute("id"));
        }
        foreach($name as $n){
            array_push($cnames, $n->textContent);
        }

        foreach($city as $c){
            array_push($cities, $c->textContent);
        }

        foreach($county as $el){
            array_push($counties, $el->textContent);
        }

        for($i =0; $i <sizeof($id); $i++){
            try {
                $input = $this->db->prepare("INSERT INTO club(clubId, clubName, city, county) VALUES (:id, :name, :city, :county)");
                $input->bindValue(':id', $id[$i], PDO::PARAM_STR);
                $input->bindValue(':name', $cnames[$i], PDO::PARAM_STR);
                $input->bindValue(':city', $cities[$i], PDO::PARAM_STR);
                $input->bindValue(':county', $counties[$i], PDO::PARAM_STR);
                $input->execute();
                echo "Success!";
            } catch (Exception $e) {
                echo "Failed to update club data in db!".'<br>';
                echo $e->getMessage();
            }
        }
    }


    /* Restores seasons/ fallyears into db  */

    public function restoreSeason(){
        $seasons = array();
        $all_seasons = $this->dom->getElementsByTagName("Season");
        foreach($all_seasons as $seas){
            array_push($seasons, $seas->getAttribute("fallYear"));
        }

        for($i = 0; $i<sizeof($seasons); $i++){
            try {
                $input = $this->db->prepare("INSERT INTO season(fallYear) VALUES (:year)");
                $input->bindValue(':year', $seasons[$i], PDO::PARAM_STR);
                $input->execute();
                echo "Success!";
            } catch (Exception $e) {
                echo "Failed to update season data in db!".'<br>';
                echo $e->getMessage();
            }
        }
    }

    /*
        Updates season_skier table with usernames of the skiers in a given
        fallYear, clubId and the total distance they skied
    */

    public function restoreTotalDist(){

        $myUserNames = array();
        $user_ses_16 = array();
        $distance_15 = array();
        $distance_16 = array();

        $xpath= new DOMXPath($this->dom);

        //Usernames who perticipated in 2015 season.

        $usernames_15 = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
        foreach($usernames_15 as $user){
            array_push($myUserNames, trim($user->textContent));
        }

        //Finding total distance for each username:
        $sum = 0;
        for($i = 0; $i< sizeof($myUserNames); $i++){
            $dist = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$myUserNames[$i].'"]/Log/Entry/Distance');
            for($j = 0; $j <$dist->length; $j++){
                $sum += ($dist->item($j)->textContent);
            }
            array_push($distance_15, $sum);
            $sum = 0;
            // echo 'Username: '.$myUserNames[$i].'    index: '.$i.' --- '.'Tot Dist: '.$distance_15[$i].'<br><br>';
        }

        //Usernames who perticipated in 2016 season.
        $usernames_16 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
        foreach($usernames_16 as $user){
            array_push($user_ses_16, trim($user->textContent));
        }

        $sum = 0;
        for($i = 0; $i< sizeof($user_ses_16); $i++){
            $dist_2 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$user_ses_16[$i].'"]/Log/Entry/Distance');
            for($j = 0; $j <$dist_2->length; $j++){
                $sum += ($dist_2->item($j)->textContent);
            }
            array_push($distance_16, $sum);
            $sum = 0;
        }



        // Inserting the data from fallYear 2015 in db.
        $ses = $xpath->query('//SkierLogs/Season/@fallYear');

        for($i = 0; $i< sizeof($myUserNames); $i++){
            try {
                $null = NULL;
                $club = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$myUserNames[$i].'"]/ancestor::Skiers/@clubId');
                $input = $this->db->prepare("INSERT INTO skier_distance(userName, fallYear, clubId, total_distance) VALUES (:user, :fall, :club, :tot_dist)");
                $input->bindValue(':user', $myUserNames[$i], PDO::PARAM_STR);
                $input->bindValue(':fall', $ses->item(0)->textContent, PDO::PARAM_STR);
                $input->bindValue(':tot_dist', $distance_15[$i], PDO::PARAM_STR);
                if($club->length){
                    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $input->bindValue(':club', $null, PDO::PARAM_STR);
                $input->execute();
            } catch (Exception $e) {
                echo "Failed to update skier_distance table in db!".'<br>';
                echo $e->getMessage();
            }

        }
        // Inserting the data from fallYear 2016 in db.

        for($i = 0; $i< sizeof($user_ses_16); $i++){
            try {
                $null = NULL;
                $club = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$user_ses_16[$i].'"]/ancestor::Skiers/@clubId');
                $input = $this->db->prepare("INSERT INTO skier_distance(userName, fallYear, clubId, total_distance) VALUES (:user, :fall, :club, :tot_dist)");
                $input->bindValue(':user', $user_ses_16[$i], PDO::PARAM_STR);
                $input->bindValue(':fall', $ses->item(1)->textContent, PDO::PARAM_STR);
                $input->bindValue(':tot_dist', $distance_16[$i], PDO::PARAM_STR);
                if($club->length){
                    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
                }else $input->bindValue(':club', $null, PDO::PARAM_STR);
                $input->execute();
            } catch (Exception $e) {
                echo "Failed to update skier_distance table in db!".'<br>';
                echo $e->getMessage();
            }
        }
    }
}
